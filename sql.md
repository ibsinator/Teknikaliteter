<!-- TITLE: SQL -->
<!-- SUBTITLE: Structured Query Language -->

Programmeringsspråk för att hantera data i relationsdatabaser. En relationsdatabas oarganiserar data i en, eller flera, tabeller. Det finns en [https://en.wikipedia.org/wiki/Relational_model| modell] som skapades 1969 av Edgar F. Codd som anger hur data kan struktureras. All data sparas som tubler och grupperas i tabeller (relationer/relations).

Relationerna är logiska kopplingar mellan tabellerna och baseras på interaktionen mellan dem. För att fungera effektivt måste transaktioner göras enligt [https://en.wikipedia.org/wiki/ACID| ACID].

| SQL          | Relationsdtabas  |
| ----------------- | ----------------------------- |
| Rad                | Tupel eller post |
|Kolumn                 | Attribut eller fält |
|Tabell                 | Relation eller "base relvar" |
| View eller result set  | Derived relvar


```sql
SELECT * FROM databasen;
```


En sekvens i SQL svslutas med semikolon (;) och är egenligen ett kommando.

```sql
CREATE TABLE tesing (
  [namn på kolumn] [datatyp],
  [namn på kolumn] [datatyp]
);
```

```sql
CREATE TABLE tesing ([namn på kolumn] [datatyp], [namn på kolumn] [datatyp]);
```

==== Datatyper ====
Data type 	Description
CHARACTER(n) 	Character string. Fixed-length n
VARCHAR(n) or
CHARACTER VARYING(n) 	Character string. Variable length. Maximum length n
BINARY(n) 	Binary string. Fixed-length n
BOOLEAN 	Stores TRUE or FALSE values
VARBINARY(n) or
BINARY VARYING(n) 	Binary string. Variable length. Maximum length n
INTEGER(p) 	Integer numerical (no decimal). Precision p
SMALLINT 	Integer numerical (no decimal). Precision 5
INTEGER 	Integer numerical (no decimal). Precision 10
BIGINT 	Integer numerical (no decimal). Precision 19
DECIMAL(p,s) 	Exact numerical, precision p, scale s. Example: decimal(5,2) is a number that has 3 digits before the decimal and 2 digits after the decimal
NUMERIC(p,s) 	Exact numerical, precision p, scale s. (Same as DECIMAL)
FLOAT(p) 	Approximate numerical, mantissa precision p. A floating number in base 10 exponential notation. The size argument for this type consists of a single number specifying the minimum precision
REAL 	Approximate numerical, mantissa precision 7
FLOAT 	Approximate numerical, mantissa precision 16
DOUBLE PRECISION 	Approximate numerical, mantissa precision 16
DATE 	Stores year, month, and day values
TIME 	Stores hour, minute, and second values
TIMESTAMP 	Stores year, month, day, hour, minute, and second values
INTERVAL 	Composed of a number of integer fields, representing a period of time, depending on the type of interval
ARRAY 	A set-length and ordered collection of elements
MULTISET 	A variable-length and unordered collection of elements
XML 	Stores XML data


{|
| '''''Värde''''' || '''''CHAR(4)''''' 	|| '''''Utrymme''''' || '''''VARCHAR(4)''''' || '''''Utrymme'''''
|-
| ''              || '    '             || 4 bytes           || ''                   || 1 byte
|
| 'ab' 	          || 'ab  '             || 4 bytes           || 'ab'                 || 3 bytes
|-
| 'abcd'          || 'abcd'             || 4 bytes           || 'abcd'               || 5 bytes
|-
| 'abcdefgh'      || 'abcd'             || 4 bytes           || 'abcd'               || 5 bytes
|}

https://dev.mysql.com/doc/refman/5.6/en/char.html

<code>CREATE TABLE</code> är en klausul (clause) eller ett kommando. Bör skrivas med VERSALER.

Parametern (det inom parentes) är en lista av kolumner, datatyper eller värden.


 <nowiki>INSERT INTO celebs (id, name, age)
VALUES (1, 'Justin Diaper',21);</nowiki>

 <nowiki>SELECT name FROM celebs;</nowiki>

 <nowiki>UPDATE celebs 
SET age = 22 
WHERE id = 1;</nowiki>

 <nowiki>ALTER TABLE celebs ADD COLUMN twitter_handle TEXT;</nowiki>

 <nowiki>UPDATE celebs
SET twitter_handle = '@taylorswift13'
WHERE id = 4;</nowiki>

 <nowiki>DELETE FROM celebs WHERE twitter_handle IS NULL; </nowiki>


    CREATE TABLE creates a new table.
    INSERT INTO adds a new row to a table.
    SELECT queries data from a table.
    UPDATE edits a row in a table.
    ALTER TABLE changes an existing table.
    DELETE FROM deletes rows from a table.



 <nowiki></nowiki>
 <nowiki></nowiki>
 <nowiki></nowiki>
 <nowiki></nowiki>
 <nowiki></nowiki>
 <nowiki></nowiki>
 <nowiki></nowiki>
 <nowiki></nowiki>
 <nowiki></nowiki>
 <nowiki></nowiki>
 <nowiki></nowiki>
 <nowiki></nowiki>
 <nowiki></nowiki>
 <nowiki></nowiki>
 <nowiki></nowiki>
 <nowiki></nowiki>
 <nowiki></nowiki>
 <nowiki></nowiki>
 <nowiki></nowiki>






• Necessär + lösa flaskor i den där påsen
• Gröna klänningen
• Nya sockorna
• Underklädr
• Korta + långa nya tights
• Svarta kjolen
• Favvotröjan
• Sol-skårten
















= Percona =
När en DML körs kommer hela klustret att låsas.